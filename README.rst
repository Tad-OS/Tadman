*************************************************
Tadman - *Some Package Manager Written in Python*
*************************************************

.. note::

  The you wish to submit an issue or are looking to fork the repo for a merge
  request, please do so via the `Gitlab repo <https://gitlab.com/Tad-OS/Tadman>`_.
  The `GitHub repo <https://github.com/KeepPositive/Tadman>`_ is only a mirror.

What It Does
============

Tadman acts as a graphical wrapper for common build systems and makes
configuring and building source code easily. It then (by default) installs
the built package in ``/usr/local/tadman/PACKAGE-NAME`` and symlinks the
files to your root directory where they belong, similiar to GNU Stow.
Uninstalling an old program is as easy as running a single command:

::

  $ sudo tadman uninstall PACKAGE-NAME

What It Does Not Do
===================

A flaw that one may note is Tadman's lack of dependency tracking; This is an
intentional design choice. While it may not be the fastest approach, Tadman
does not wish to make choices for the user like a majority of other package
managers.

With Tadman, there are no package maintainers or unnecessary dependencies.
Rather, it gives the user direct access to the world's largest and most
up-to-date software library: the internet. It allows users to install
software from a Git repo and uninstall it easily. All you need to do is find
some source code, and install it using a single command:

::

  $ sudo tadman build PACKAGE-NAME

Dependencies
============

The only necessary dependencies for Tadman are:

* `click <http://click.pocoo.org/6/>`_ >= 6.0 (for command line parsing)
* GNU Make
* Python >= 3.4 (built with curses support)
* `Setuptools <https://github.com/pypa/setuptools>`_ module for Python 3

Though, there are also several optional dependencies:

* Autotools (to build Autotools-based projects)
* CMake (to build CMake-based projects)
* `docutils <http://docutils.sourceforge.net/>`_ (to generate the manual page)
* `pytest <https://docs.pytest.org/en/latest/>`_ (to the tests)

.. note::

  You should make an attempt to install the Python dependencies through your
  system's package manager. But if the packages are not includes in your package
  manager's repo, you may install the Python dependencies using 
  `Pip <https://pip.pypa.io/en/stable/>`_. Just run the following command:

  ::

    $ pip install -r requirements.txt

  By default, this will install click, docutils and pytest. If you would
  like to not install optional dependencies, you should just run:

  ::

    $ pip install click

Installation
============

Tadman can be installed using Git and Python Setuptools. It is suggest that
you use the following commands on a UNIX-like system:

::

  $ git clone https://github.com/KeepPositive/Tadman.git
  $ cd Tadman
  $ python3 setup.py install

.. note::

  Currently, use of the ``--user`` flag does not work since Tadman requires
  root access for its installation.

You should now have an executable named ``tadman`` in your path.

Manpage
-------

While Tadman does provide nicely formatted help statements for its entire
command line interface, there is also a manpage available for installation.
If you have docutils installed, you can build and install the manpage by
running the following command with root access:

::
  
  $ python3 setup.py install_manpage

Usage
=====

Tadman should make building source code packages easier. Try it out by
downloading some source code that uses autotools
(has an *autogen.sh* or a *configure* file) or CMake (has a *CMakeLists.txt*),
and then run the following command:

::

  $ python3 tadman build

A curses-based GUI will open, and prompt you for the package name and version
(necessary for better book-keeping of the installation). Next, an options list
will appear, where you can check off the options you want, or leave the ones
you don't blank. Once you are ready, hit 'e', and assuming you have all of
the dependencies your program requires, the program will be installed.

For a complete list of available arguments, check out
`the manpage <docs/tadman.man.adoc>`_.

Tutorial
========

For the sake of this short tutorial, we will be manipulating the imaginary
package called ``foo`` using Tadman. We will begin by uninstalling the
outdated version ``foo-1.2.2``, then building the new ``foo-1.2.3`` from
source, and finally reverting back to ``foo-1.2.2``.

Uninstall
---------

In order to evade any collisions with our new version of ``foo``, we will
uninstall ``foo-1.2.2``. It is as easy as running the following command:

::

  $ tadman uninstall foo-1.2.2

Now ``foo-1.2.2`` has been unlinked from the user's *.local/bin*, but
the compiled program still exists in *.local/tadman*.


Build
-----

Now we want to build the newly released **foo-1.2.3**. To do so, we will
unpack/clone the sources, change to the sources' directory, and then look at
the available files:

::

  $ ls
  configure   docs    foo.c     foo.h     README.md

Here, we see that package contains a *configure* file. When it comes to
building, almost any package with a *autogen.sh*, *configure* or
*CMakeLists.txt* file can be automated by Tadman (assuming you have the
correct dependencies and whatnot). Now, we can run Tadman:

::

  $ tadman build

After a moment, the user will be prompted with a Curses based interface
from which they can toggle option flags, edit installation paths, etc.
At first, you be asked to provide a name and version number for the
package. Tadman will attempt to create sensible defaults for these,
but you can enter whatever you want. In this case, we will enter *foo*
for the name, and *1.2.3* for the version numbers. Now we can edit
package options. At any time you can press ``?`` for a help screen if
necessary. After selecting options, the user can press ``e`` to execute the
build process. If the build fails for some reason, the user will see an
error. But if the build and installation are successful, then the user will
be prompted to install their newly created software!

.. note::

  Tadman's default is to install packages on a per-user basis. To
  access your installed package, ensure *$HOME/.local/tadman* is in your
  user's **PATH** environment variable.

Install
-------

Oh no! Our new ``foo-1.2.3`` package has a major security bug! But since we
would like to continue using ``foo``, we will revert back to our older
version. It is as simple as running the following commands:

::

  $ tadman uninstall foo-1.2.3
  $ tadman install foo-1.2.2

Management
----------

Tadman includes several tools to manage your packages. One of these is
the **list** command. It will print a formatted table of all available
packages. 

::

  $ tadman list

  Name       Owner  State        Latest Activity
  ---------- ------ ------------ --------------------
  foo-1.2.2  User   Installed    2017-04-20 22:13:09 
  foo-1.2.3  User   Uninstalled  2017-04-20 21:48:29 
  ---------- ------ ------------ --------------------

The **info** command will print information about a specific package.

::

  $ tadman info foo-1.2.2

  Name: foo-1.2.2
  Directory: /home/tmose1106/.local/tadman/foo-1.2.2
  State: Installed
  History:
    * Built at 2017-02-23 10:25:56
    * Installed at 2017-02-23 10:26:11
    * Uninstalled at 2017-04-20 21:37:06
    * Installed at 2017-04-20 22:13:09
  Build Options:
    * --enable-bar
    * --mandir=man/man1

Licensing
=========

Copyright © 2016-2017 Ted Moseley. Free use of this software is granted under
the terms of the `MIT Open Source License <https://opensource.org/licenses/MIT>`_.
