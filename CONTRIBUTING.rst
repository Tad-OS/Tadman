***************************
Contributing to the Project
***************************
:Author: Ted Moseley
:Version: 0.0.1

Code
====

Right now, not much to say here other than follow
`PEP8 <https://www.python.org/dev/peps/pep-0008/>`_ minus the code line length
rule. This does not mean make 200 character lines though. I would say keep it
at `90-ish <https://www.youtube.com/watch?v=wf-BqAjZb8M>`_.

Also, I would highly suggest taking advantage of
`flake8 <https://gitlab.com/pycqa/flake8?>`_. It does an extremely good job
helping to *PEP8* your code without being too picky. There are also lots of
great text editor plugins for flake8.

Documentation
=============

For documentation, I require that it be in the Python standard format of
`reStructuredText <http://docutils.sourceforge.net/rst.html>`_. I have a
few things to ask when writing in reStructuredText:

* Please keep the line length under 80 characters. Think about all of your
  friends who still use
  `punched cards for computing <https://en.wikipedia.org/wiki/Punched_card>`_
  Anyways, this isn't code, so there isn't really any excuse other than long
  urls, perhaps.
* Perhaps one of my biggest pet-peeves is when people create reStructuredText
  documents, and give them a *.txt* extension. While reStructuredText is a
  **form** of plain text document, it is **NOT** just plain text. Do you see
  people creating Markdown files with *.txt* extensions? I have not. All it
  does is confuse people and it does not render correcly on PyPi, GitHub,
  Gitlab, etc. So please, give reStructuredText documents *.rst* extensions,
  and not *.txt* extensions.
* I ask that you follow the `heading standards proposed by the Python
  Documentation <https://docs.python.org/devguide/documenting.html#sections>`_
  for the sake of keeping things somewhat standard. How I like to do section
  headers for documents like this is as follows:

  * ``*`` with overline, for a title
  * ``=`` without overline, for sections
  * ``-`` without overline, for subsections

  Following those, you are getting into wild territory. The documentation
  suggests using the ``^`` character next. I personally think the ``+`` is
  more in-line with the general theme of the characters, but I'm not in charge
  of the format, so....

* Just because you see it in a tutorial or blog post relating to
  reStructuredText, it does not necessarily mean it is valid vanilla Docutils
  reStructuredText. Projects like Sphinx make many additions to the format,
  making it quite confusing at times what is actual reStructuredText and
  what is not.
* Before commiting, please ensure that your reStructuredText documents
  build correctly. I suggest using the ``rst2html5.py`` command provided
  by Docutils to generate pages which you can view for errors in your
  browser. Please fix any error message that is generated during
  the parsing of the reStructuredText document.
* Finally, if you are going to mention reStructureText, please stylize
  it correctly as I have throughout this document. The developers had
  to put a message about its spelling on their homepage for crying out
  loud! Do them a solid.

Now you are writing reStructureText like a pro!
