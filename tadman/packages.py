""""""
# Standard
import os
import subprocess
# Project
from tadman import autotools, cmake


def get_package_options(a_path):
    """"""
    if os.path.isfile("{}/configure".format(a_path)):
        build_tuple = ("autotools", autotools)
    elif os.path.isfile("{}/CMakeLists.txt".format(a_path)):
        build_tuple = ("cmake", cmake)
    elif os.path.isfile("{}/autogen.sh".format(a_path)):
        build_tuple = ("autogen", autotools)

    if build_tuple[0] == "autogen":
        subprocess.run(["./autogen.sh"])
        output_build_type = "autotools"
    else:
        output_build_type = build_tuple[0]

    option_list, install_list = autotools.get_config_lists(a_path)
    option_dict = autotools.option_processor(option_list)
    install_dict = autotools.install_flag_processor(install_list)

    return output_build_type, option_dict, install_dict
