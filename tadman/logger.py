"""
This script is an experiment in logging the installation state
of each package.
"""
# Standard
import datetime
import json
import os
import time


def _get_log_path(package_path, file_name="tadman.json"):
    """"""
    return os.path.join(package_path, file_name)


def _open_log_file(file_path):
    """"""
    try:
        return json.load(open(file_path))
    except FileNotFoundError:
        print("Log file {} not found!".format(file_path))


def _write_log_file(file_path, new_contents):
    """"""
    try:
        json.dump(new_contents, open(file_path, 'w'))
    except FileNotFoundError:
        print("Log file {} not found!".format(file_path))


def create_log_file(package_path, mode, **kwargs):
    """"""
    log_template = {
        'history': [(mode, datetime.datetime.now().isoformat())],
    }
    # Add all keyword arguments to the template
    log_template.update(dict(kwargs))

    log_file_path = _get_log_path(package_path)

    _write_log_file(log_file_path, log_template)


def update_log_file(package_path, mode):
    """""" 
    log_file_path = _get_log_path(package_path)
    log_contents = _open_log_file(log_file_path)
    timestamp = (mode, datetime.datetime.now().isoformat())
    # Add the timestamp to history
    log_contents['history'].append(timestamp)
    # Alter the installation state variable
    if mode == "install":
        log_contents['installation_state'] = True
    elif mode == "uninstall":
        log_contents['installation_state'] = False
    # Write the new contents
    _write_log_file(log_file_path, log_contents) 
