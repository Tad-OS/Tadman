""""""
# Standard
import os
import sys


def check_for_root(a_message="Only root can execute this command."):
    """
    Check whether the shell running the script has root user access.
    If not, end the script and print an error message.
    """
    if os.geteuid() != 0:
        print(a_message)
        sys.exit(1)


def confirmation_prompt(a_message, default=False):
    """
    This function simply prompts the user with the message *message*
    follow by the ability to input a "yes" or "no" response. If
    *default* is True, the default value will be "yes", i.e. if the
    user enters an empty string, the function will assume the user
    chose "yes".
    """
    yes_list = ['y']
    no_list = ['n']

    if default:
        option_string = "Y/n"
        yes_list.append('')
    else:
        option_string = "y/N"
        no_list.append('')

    while True:
        choice = input("{} [{}] ".format(a_message, option_string)).lower()

        if choice in yes_list:
            return True
        elif choice in no_list:
            return False
        else:
            print("Invalid option. Please respond {}".format(option_string))
            continue


def remove_empty_folders(a_path):
    """
    This method removes everything within a directory. Great stuff.

    Credit for this function goes to Jacob Tomlinson. Thanks!
    Check it out his website at: https://www.jacobtomlinson.co.uk
    """
    if not os.path.isdir(a_path):
        return

    # Remove empty subfolders
    files = os.listdir(a_path)
    if files:
        for a_file in files:
            full_path = os.path.join(a_path, a_file)
            if os.path.isdir(full_path):
                remove_empty_folders(full_path)
