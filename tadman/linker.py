"""
This function contains functions which help to link packages to
the user's root directory. It also unlinks them. Which ever you
prefer.
"""
# Standard
import os


def link_directory(package_path, install_directory):
    """
    This function walks through a directory, and recreates its structure
    by recreating the subdirectories within the root directory. For each file,
    it creates a symlink to the file within the directories created. Thus, the
    entire file structure is recreated within the root.
    """
    for root, dirs, files in os.walk(package_path):
        # For each subdirectory within the starting directory
        for a_dir in dirs:
            absolute_path = os.path.join(root, a_dir)
            relative_path = os.path.relpath(absolute_path, package_path)
            new_path = os.path.join('/', relative_path)

            # print("Abs:", absolute_path)
            # print("Rel:", relative_path)
            # print("New:", new_path)

            if not os.path.isdir(new_path):
                os.mkdir(new_path)
                print("Created directory:", new_path)
                pass
            else:
                print("Directory \'{}\' already exists".format(new_path))
                pass

        # For each file within the starting directory
        for a_file in files:
            if a_file == 'tadman.json':
                continue

            absolute_path = os.path.join(root, a_file)
            relative_path = os.path.relpath(absolute_path, package_path)
            new_path = os.path.join('/', relative_path)

            # print("Rel:", relative_path)

            try:
                os.symlink(absolute_path, new_path)
                pass
                print("{} --> {}".format(new_path, absolute_path))
            except FileExistsError:
                print("File \'{}\' already exists".format(new_path))
                pass


def unlink_directory(package_path):
    """
    This function removes any exisiting links that were created by the
    sym_farm function while installing a package. It is almost like
    uninstall the package so to speak.
    """

    unlink_path = package_path
    sub_length = len(unlink_path)

    for root, dirs, files in os.walk(package_path):
        # For each file within the starting directory
        for a_file in files:
            # print(name)
            if a_file == 'tadman.json':
                continue

            absolute_path = os.path.join(root, a_file)
            relative_path = os.path.relpath(absolute_path, package_path)
            new_path = os.path.join('/', relative_path)

            if os.path.islink(new_path):
                os.remove(new_path)
                print("{} -X> {}".format(new_path, absolute_path))
            else:
                print("File {} is not a link".format(new_path))

        for a_dir in dirs:
            absolute_path = os.path.join(root, a_dir)
            relative_path = os.path.relpath(absolute_path, package_path)
            new_path = os.path.join('/', relative_path)

            if not os.path.isdir(new_path):
                print("Directory \'{}\' does not exist".format(new_path))
            elif os.listdir(new_path):
                print("Directory \'{}\' is not empty".format(new_path))
            else:
                os.rmdir(new_path)
                print("Removing directory:", new_path)
