"""
This is the main script for the Tadman package manager. All of the
commands can be found within their own function. The naming scheme
is typically '{command}_package'.
"""
# Standard
import os
import subprocess
import sys
# Dependency
import click
# Project
import tadman
from tadman import (curses_interface, linker, logger, main, packages,
                    paths, utils)

CORES = 4
SYSTEM_LINK_PATH = "/usr/local"
SYSTEM_INSTALL_PATH = "/usr/local/tadman"
USER_LINK_PATH = os.path.join(os.getenv("HOME"), ".local")
USER_INSTALL_PATH = os.path.join(USER_LINK_PATH, "tadman")
EPILOG_STRING = """Copyright © 2016-2017 Ted Moseley. Free use of this
software is granted under the terms of the MIT Open Source License
available at <https://opensource.org/licenses/MIT>."""


@click.group(epilog=EPILOG_STRING)
@click.version_option(tadman.__version__)
def cli():
    """ Tadman - Some package manager written in Python """
    pass


@cli.command(epilog=EPILOG_STRING,
             help="Build a package in DIRECTORY.",
             name="build")
@click.argument("directory", default='.', required=False)
@click.option("--system", "system_wide", is_flag=True, default=False,
              help="Run a system wide build (requires root)")
def build_package(directory, system_wide):
    """
    This function is responsible for finding all of the necessary
    information and requested options for building packages.

    * The *directory* argument reflects the directory where the root
      of the project to be built resides. If no path is passes, it
      is assumed that the current directory is the root.
    * The *system_wide* argument defines whether to build and install
      the package system-wide. It requires root user.
    """
    # Make sure a path to directory is available and exists
    input_path = paths.last_slash_check(directory)
    package_path = os.path.abspath(input_path)

    if os.path.isdir(package_path):
        # Change to the source directory
        os.chdir(input_path)
    else:
        print("Invalid directory entered.")
        sys.exit(1)

    # Ensure the user running the script is the root user
    if system_wide:
        utils.check_for_root()
        installation_path = SYSTEM_INSTALL_PATH
        linking_path = SYSTEM_LINK_PATH
    else:
        installation_path = USER_INSTALL_PATH
        linking_path = USER_LINK_PATH

    # Get info from the package
    package_name, package_version = paths.name_version_split(package_path)
    build_type, option_dict, install_dict = packages.get_package_options(package_path)
    # Run the curses-based user interface using the info from above
    build_context = curses_interface.main_loop(option_dict, install_dict,
                                               package_name, package_version,
                                               build_type)
    # Guard for failed or exited configuration process
    if not build_context:
        print("Tadman exited.")
        sys.exit(0)

    # Generate the options from the selection indexes
    package_name, package_version, option_flags, install_flags = build_context
    if build_type == 'autotools':
        configure_list = ["./configure", "--prefix={}".format(linking_path)]
    elif build_type == 'cmake':
        configure_list = ["cmake",
                          "-DCMAKE_INSTALL_PREFIX={}".format(linking_path)]

    for install_flag in install_flags:
        configure_list.append(install_flag)

    for option_flag in option_flags:
        configure_list.append(option_flag)

    package_string = "{}-{}".format(package_name, package_version)

    output_directory = os.path.join(installation_path, package_string)
    # Finally, build the package
    # Run the configuration command with options
    config_process = subprocess.run(configure_list)

    if config_process.returncode != 0:
        print("Configuration failed! Please read the output thoroughly.")
        sys.exit(1)

    # Build the sources and install the sources in a tadman directory
    build_process = subprocess.run(["make", "-j", str(CORES)])

    system_list = ["make", "DESTDIR={}".format(output_directory), "install"]

    install_process = subprocess.run(system_list)

    build_status = build_process.returncode
    install_status = install_process.returncode

    if build_status != 0 and install_status == 0:
        print("Build had a warning, but installation succeeded.")
    elif build_status != 0 and install_status != 0:
        print("Build failed. Please read above output thoroughly.")
        sys.exit(1)
    elif install_status != 0:
        print("Installation failed. Please read above output thoroughly.")
        sys.exit(1)

    # Log the successful installation
    logger.create_log_file(output_directory, "build",
                           option_flags=option_flags,
                           install_flags=install_flags)
    # Prompt user if ready for link installation
    if utils.confirmation_prompt("Would you like to install this package now?",
                                 default=True):
        command_list = ["tadman", "install", "--path", output_directory]
        # If system wide is desired, add sudo and --system flag

        if system_wide:
            # command_list.insert(0, "sudo")
            command_list.insert(3, "--system")
        subprocess.run(command_list)

    print("Compiled sources installed in:", package_path)

@cli.command(epilog=EPILOG_STRING, name="install")
@click.argument("package_name")
@click.option("--path", "use_path", is_flag=True,
              help="Use package's path rather than name")
@click.option("--system", "system_wide", is_flag=False,
              help="Run a system wide installation (requires root)")
def install_package(package_name, use_path, system_wide):
    """ Install package of name NAME. """
    # Ensure the user running the script is the root user if system install
    if system_wide:
        utils.check_for_root()
        install_directory = SYSTEM_LINK_PATH
    else:
        install_directory = USER_LINK_PATH

    # Make sure a path to directory is available and exists
    if use_path:
        package_path = package_name
    else:
        package_path = paths.get_package_path(package_name, (SYSTEM_INSTALL_PATH,
                                                             USER_INSTALL_PATH))

    # Link all of the files to their respective places
    linker.link_directory(package_path, install_directory)


@cli.command(epilog=EPILOG_STRING,
             help="List all packages or details about a single package.",
             name="list")
@click.argument("package_name", required=False)
def list_packages(package_name):
    """
    If the user enters provides a package name argument, print out the
    contents of the package. If no package name argument is provided,
    then print the names of all available packages.
    """
    if package_name:
        package_dir = paths.get_package_path(package_name, (SYSTEM_INSTALL_PATH,
                                                            USER_INSTALL_PATH))
        if package_dir:
            remove_length = len(package_dir)
            for root, _, files in os.walk(package_dir):
                for name in files:
                    print(os.path.join(root, name)[remove_length:])
        else:
            print("Package {} does not exist.".format(package_name))
    else:
        for install_path in (SYSTEM_INSTALL_PATH, USER_INSTALL_PATH):
            try:
                for a_path in os.listdir(install_path):
                    if os.path.isdir(os.path.join(install_path, a_path)):
                        print(a_path)
            except FileNotFoundError:
                continue


@cli.command(epilog=EPILOG_STRING, name="uninstall")
@click.argument("package_name")
@click.option("--path", "use_path", is_flag=True,
              help="Use package's path rather than name")
@click.option("--system", "system_wide", is_flag=False,
              help="Uninstall a system wide package (requires root)")
def uninstall_package(package_name, use_path, system_wide):
    """ Uninstall package of name NAME. """
    # Ensure the user running the script is the root user
    if system_wide:
        utils.check_for_root()
    # Make sure a path to directory is available and exists
    if use_path:
        package_path = package_name
    else:
        package_path = paths.get_package_path(package_name, (SYSTEM_INSTALL_PATH,
                                                             USER_INSTALL_PATH))

    # Remove all of the symlinks
    linker.unlink_directory(package_path)
    if utils.confirmation_prompt("Would you like to remove this package now?"):
        utils.remove_empty_folders(package_path)
