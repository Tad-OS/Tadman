# Standard
import os
import sys
# Dependency
try:
    import docutils.core
except ImportError:
    pass
import setuptools
# Project
import tadman


class InstallManpage(setuptools.Command):
    """
    This class adds functionality for build a UNIX manpage for
    Tadman. You can view and edit it in the file 'tadman.1.rst`.
    """
    description = "Build manpage using docutils and install it"
    user_options = []
    boolean_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    @staticmethod
    def __parse_manpage(string):
        return docutils.core.publish_string(source=string,
                                            writer_name="manpage")

    def run(self):

        if not docutils.core:
            print("Please install docutils to build the manpage")
            sys.exit(0)

        input_file = os.path.abspath("tadman.1.rst")
        output_file = "/usr/share/man/man1/tadman.1"

        with open(input_file) as a_file:
            print("Opening reStructuredText file {}".format(input_file))
            contents = a_file.read()

        processed = self.__parse_manpage(contents)
        print("Processed manpage file using docutils")

        with open(output_file, 'w') as a_file:
            print("Writing output file {}".format(output_file))
            a_file.write(processed.decode())

        print("Manpage installed!")


setuptools.setup(
    author="Ted Moseley",
    author_email="tmoseley1106@gmail.com",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console :: Curses",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: MacOS",
        "Operating System :: POSIX :: BSD",
        "Operating System :: POSIX :: Linux",
        "Operating System :: POSIX :: Other",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: System :: Installation/Setup",
        "Topic :: System :: Systems Administration",
    ],
    # For the manpage command
    cmdclass={
        "install_manpage": InstallManpage,
    },
    description="Some package manager written in Python",
    entry_points={
          "console_scripts": ["tadman = tadman.main:cli"],
    },
    name='tadman',
    packages=["tadman"],
    url="https://gitlab.com/Tad-OS/Tadman",
    version=tadman.__version__,
)
